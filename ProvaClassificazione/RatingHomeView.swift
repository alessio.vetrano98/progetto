//
//  RatingHomeView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 29/10/2020.
//

import SwiftUI

struct RatingHomeView: View {
    @Binding var rating2: Int?
    var indexFilm: Int
    @ObservedObject var getFilm = GetFilms.shared
    
    var max: Int = 5
    var body: some View {
        HStack {
            
            ForEach(1...max, id: \.self) { index in
                
                Image(systemName: self.starType(index: index))
                    .foregroundColor(Color.orange)
                    .onTapGesture {
                        self.rating2 = index
                        getFilm.films[indexFilm].rating2 = rating2
                    }
                
            }
        }    }
    
    private func starType(index: Int) -> String {
        
        if let rating2 = rating2 {
            return index <= rating2 ? "star.fill" : "star"
        } else {
            return "star"
        }
        
    }

}

struct RatingHomeView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            Text("")
            Text("")
        }
    }
}
