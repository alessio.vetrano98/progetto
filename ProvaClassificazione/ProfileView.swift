//
//  ProfileView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 27/10/2020.
//

import SwiftUI

struct ProfileView: View {
    @State var progressValue1: Float = 0.0
    @State var progressValue2: Float = 0.0
    @State var progressValue3: Float = 0.0
    @State private var showModal = false
    @ObservedObject var getFilm = GetFilms.shared
    @State var imageAvatar = ""
    
//    var imageUserDefaults: String
    
    
    let userDefaults = UserDefaults.standard

    init(){
        _imageAvatar = State(initialValue: UserDefaults.standard.string(forKey: "image") ?? "")
    }
    var body: some View {
        
        NavigationView {
            VStack {
                Button(action: {
                    self.showModal.toggle()
                }, label: {
                    
                    Image(imageAvatar)
//                        .renderingMode(.original)
                        .resizable()
                        .padding()
                        .frame(width: 250, height: 320, alignment: .center)
                    
                }).sheet(isPresented: $showModal, content: {
                    AvatarModalView(imageAvatar: $imageAvatar, dismiss: {
                        self.showModal.toggle()

                        
                    })
                })

                
//
                
                
                VStack {
                    Text("Achievements")
                        .font(Font.body.bold())
                        .foregroundColor(Color("ColorAppGiallo"))
                    
                        
                    HStack {
                        VStack {
                            
                            
                            
                            
                            ProgressBar(progress: .constant(Float(getFilm.preferiti.count)/20))
                                .frame(width: 100.0, height: 100)
                                .padding(10)
                                .overlay(
                                    Image("Oro")
                                        .resizable()
                                        .frame(width: 50, height: 50)
                                )
                            
                            Text("\(Int(getFilm.preferiti.count))/20")
                                .multilineTextAlignment(.center)
                                .font(Font.body.bold())
                            
                        }
                        VStack {
                            
                            
                            ProgressBar(progress: .constant(Float(getFilm.preferiti.count)/10))
                                .frame(width: 100.0, height: 100)
                                .padding(10.0)
                                .overlay(
                                    Image("Argento")
                                        .resizable()
                                        .frame(width: 50, height: 50)
                                )
                            
                            Text("\(Int(getFilm.preferiti.count))/10")
                                .multilineTextAlignment(.center)
                                .font(Font.body.bold())
                            
                            
                        }
                        
                        
                        VStack {
                            
                            ProgressBar(progress: .constant(Float(getFilm.preferiti.count)/5))
                                
                                .frame(width: 100.0, height: 100)
                                .padding(10.0)
                                .overlay(
                                    Image("Bronzo")
                                        .resizable()
                                        .frame(width: 50, height: 50)
                                )
                            Text("\(Int(getFilm.preferiti.count))/5")
                                .font(Font.body.bold())
                                .multilineTextAlignment(.center)
                            
                        }
                        
                    }
                }
            }
            .preferredColorScheme(.dark)
            .padding(.top, 5.0)
            .navigationTitle("Profile")
            
        }
    }
    
    func loadImage() {
        
        imageAvatar = userDefaults.string(forKey: "image")!
        
    }
    
    
}
struct ProgressBar: View {
    @Binding var progress: Float
    
    var body: some View {
        
        ZStack {
            Circle()
                .stroke(lineWidth: 1.0)
                .opacity(0.2)
                .foregroundColor(Color.gray)
            
            
            
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 10.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(Color("ColorAppGiallo"))
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.linear)
            
        }
    }
}
extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}



struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
