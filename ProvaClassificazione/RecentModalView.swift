//
//  RecentModalView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 29/10/2020.
//

import SwiftUI

struct RecentModalView: View {
    
    var filmName: String
    var filmDescription: String
    var platform1: String
    var platform2: String
    var platform3: String
    var platform4: String
    
    
    
    var body: some View {
        VStack {
            VStack {
                
                Image(filmName)
                    .resizable()
                    .frame(width: 306, height: 400)
                    .cornerRadius(10)
                    .padding()
                
                ScrollView(.vertical) {
                    Text(filmDescription)
                        .padding()
                        .font(.system(size: 12))
                        .foregroundColor(.white)
                        .multilineTextAlignment(.leading)
                        .frame(width: 400)
                    
                    
                    VStack {
                        Text("Available on: ")
                            .font(Font.body.bold())
                        
                        HStack(){
                            Spacer(minLength: 20)
                            
                            Image(platform1)
                                .resizable()
                                .padding(.leading)
                                .frame(width: 120, height: 120)
                            Image(platform2)
                                .resizable()
                                .padding()
                                .frame(width: 120, height: 120)
                            Image(platform3)
                                .resizable()
                                .padding()
                                .frame(width: 120, height: 120)
                            Image(platform4)
                                .resizable()
                                .padding()
                                .frame(width: 120, height: 120)
                        }
                        
                    }
                }
            }.preferredColorScheme(.dark)
        }.padding()
    }
}


struct RecentModalView_Previews: PreviewProvider {
    static var previews: some View {
        RecentModalView(filmName: "Immagine", filmDescription: "Descrizione", platform1: "Netflix", platform2: "PrimeVideo", platform3: "GooglePlay", platform4: "Youtube")
            .preferredColorScheme(.dark)
    }
}
