//
//  RatingView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 28/10/2020.
//

import SwiftUI

struct RatingView: View {
    
    @Binding var rating: Int?
    @Binding var maxTime: Int
    @Binding var showAlert: Bool
    var indexFilm: Int
    @ObservedObject var getFilm = GetFilms.shared
    
    var max: Int = 5
    var body: some View {
        HStack {
            ForEach(1...max, id: \.self) { index in
                
                Image(systemName: self.starType(index: index))
                    .foregroundColor(Color("ColorAppGiallo"))
                    .onTapGesture {
                        self.rating = index
                        getFilm.films[indexFilm].rating = rating
                        
                        maxTime += 1
                        
                        if maxTime >= 3 {
                            showAlert = true
                            UserDefaults.standard.setValue(true, forKey: "launchedBefore")
                        }
                    }
                
            }
        }
    }
    
    
    private func starType(index: Int) -> String {
        
        if let rating = rating {
            return index <= rating ? "star.fill" : "star"
        } else {
            return "star"
        }
        
    }
}


struct RatingView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            Text("")
            Text("")
        }
    }
}
