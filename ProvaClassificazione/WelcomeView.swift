//
//  WelcomeView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 28/10/2020.
//

import SwiftUI

struct WelcomeView: View {
    @ObservedObject var getFilm = GetFilms.shared
    @State private var rating = [Int?](repeating: 0, count: GetFilms.shared.films.count)
    @State var maxTime: Int = 0
    @State var showDetail: Bool = false
    @State var showAlert: Bool = false
    @Binding var launchedBefore: Bool

    
    
    var body: some View {
        
        
        NavigationView{
            
            VStack{
                
                Text("Rate your favourites (max 3)")
                    .font(.system(size: 24)).bold()
                    .multilineTextAlignment(.leading)
                    .foregroundColor(Color("ColorAppGiallo"))
                
                ScrollView(.horizontal){
                
                HStack{

                ForEach(getFilm.films.indices) { item in
                    
                        VStack {
                            Text(getFilm.films[item].name)
                                .font(Font.body.bold() )
                                .foregroundColor(Color.white)
                            Image(getFilm.films[item].name)
                                .resizable()
                                .frame(width: 280, height: 400, alignment: .center)
                                .cornerRadius(10)
                                .padding()
                            
                            
                            //RATING VARIABILE DA PASSARE AL MACHINE LEARNING
                            RatingView(rating: $rating[item], maxTime: $maxTime, showAlert: $showAlert, indexFilm: item, max: 5)
                            
                        }
                    }
                }
                }
//                .frame(height: 500)
                .padding()
            .navigationTitle("Welcome!")
            .alert(isPresented: $showAlert, content: {
                Alert(title: Text("Alert!"), message: Text("Are you sure?"),
                      primaryButton: Alert.Button.default(Text("Yes"), action:{
                        
                        UserDefaults.standard.set(true, forKey: "launchedBefore")
                        self.launchedBefore.toggle()
                      }),
                      secondaryButton: Alert.Button.cancel(Text("No"))
                )
            })
        }.preferredColorScheme(.dark)
        }
    }
}



struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView( launchedBefore: .constant(false))
            .preferredColorScheme(.dark)
    }
}
