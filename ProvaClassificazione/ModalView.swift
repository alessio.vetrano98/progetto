//
//  ModalView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 26/10/2020.
//
import SwiftUI


struct ModalView: View {
    // 1.
//    @Binding var showModal: Bool
    var filmName: String
    var filmPic: String
    var filmDescription: String
    
    private var index: Int? {
        
        get {
            getFilm.films.firstIndex(where: {
                $0.name == filmName
                
            })

        }
    }
    
    @ObservedObject var getFilm = GetFilms.shared
    
//    @Binding var showModal: Bool

    var dismiss: () -> ()
    
    var body: some View {

        
            VStack {
                HStack{
                Spacer()
                    
                Button(action: {

                    
                    getFilm.preferiti.append(getFilm.films[index ?? 0])
                    
                    dismiss()

                        }, label: {
            
                    Image(systemName: "plus.circle")
                        
                        .foregroundColor(!getFilm.preferiti.contains(getFilm.films[index ?? 0]) ? Color("ColorAppGiallo") : .gray)
                        .imageScale(.large)
                        
                        }).padding()
                        .disabled(getFilm.preferiti.contains(getFilm.films[index ?? 0]))
                }
            }
                
            
            ScrollView{
                VStack {
                    
                    
                    Image("\(filmPic)")
                        .resizable()
                        .frame(width: 306, height: 400)
                        .cornerRadius(10)
                    Text(filmName)
              
                    RatingView(rating: $getFilm.films[index ?? 0].rating, maxTime: .constant(999)
                               , showAlert: .constant(false), indexFilm: index ?? 0)
                    
                    Text(getFilm.films[index ?? 0].descriptionFilm)
                        .padding()
                        .foregroundColor(.white)
                    
                    

                    
                    
                }.preferredColorScheme(.dark)
            }
        }

    
}


struct ModalView_Previews: PreviewProvider {
    static var previews: some View {
//   ANTEPRIMA NON FUNZIONA
       EmptyView()
        
    }
}
