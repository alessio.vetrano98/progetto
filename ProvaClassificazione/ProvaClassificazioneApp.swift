//
//  ProvaClassificazioneApp.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 22/10/2020.
//

import SwiftUI

@main
struct ProvaClassificazioneApp: App {
    @State var launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
    
    var body: some Scene {
        
        WindowGroup {
            if launchedBefore {
                ContentView()
                
            } else {
                WelcomeView( launchedBefore: $launchedBefore)

        }
    }
}
}
