//
//  ProvaView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 29/10/2020.
//

import SwiftUI

struct ProvaView: View {
    @State var progressValue1: Float = 0.0
    @State var progressValue2: Float = 0.0
    @State var progressValue3: Float = 0.0
    
    var body: some View {
        ZStack {
            /*Color.yellow
                .opacity(0.1)
                .edgesIgnoringSafeArea(.all)*/
            
            VStack {
                HStack {
                    
                ProgressBar(progress: self.$progressValue1)
                    .frame(width: 150.0, height: 150.0)
                    .padding(40.0)
                    
                    .overlay(
                        Image("Bronzo")
                            .resizable()
                            .frame(width: 100, height: 100, alignment: .center)
                    )
                Button(action: {
                    self.incrementProgress1()
                }) {
                        Image(systemName: "plus.rectangle.fill")
                        Text("Increment")
                    .padding(15.0)
                    .overlay(
                        RoundedRectangle(cornerRadius: 15.0)
                            .stroke(lineWidth: 2.0)
                    )
                }
                
                Spacer()
                }
                
                HStack{
                    
                    ProgressBar(progress: self.$progressValue2)
                        .frame(width: 150.0, height: 150.0)
                        .padding(40.0)
                    Image("TrofeoBronzo")
                    
                    Button(action: {
                        self.incrementProgress2()
                    }) {
                            Image(systemName: "plus.rectangle.fill")
                            Text("Increment")
                        .padding(15.0)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15.0)
                                .stroke(lineWidth: 2.0)
                        )
                    }
                    
                    Spacer()
                }
                HStack {
                    
                    ProgressBar(progress: self.$progressValue3)
                        .frame(width: 150.0, height: 150.0)
                        .padding(40.0)
                    Image("TrofeoBronzo")
                    
                    Button(action: {
                        self.incrementProgress3()
                    }) {
                            Image(systemName: "plus.rectangle.fill")
                            Text("Increment")
                        .padding(15.0)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15.0)
                                .stroke(lineWidth: 2.0)
                        )
                    }
                    
                    Spacer()
                }
            }
            }
    }
    
    func incrementProgress1() {
        let randomValue = Float([0.012, 0.022, 0.034, 0.016, 0.11].randomElement()!)
        self.progressValue1 += randomValue
    }
    func incrementProgress2() {
        let randomValue = Float([0.012, 0.022, 0.034, 0.016, 0.11].randomElement()!)
        self.progressValue2 += randomValue
    }
    func incrementProgress3() {
        let randomValue = Float([0.012, 0.022, 0.034, 0.016, 0.11].randomElement()!)
        self.progressValue3 += randomValue
    }
}




struct ProvaView_Previews: PreviewProvider {
    static var previews: some View {
       ProvaView()
        .preferredColorScheme(.dark)
    }
}
