//
//  SettingView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 27/10/2020.
//

import SwiftUI

struct SettingView: View {
    var body: some View {
        NavigationView {
            
            VStack {
                List {
                    Text("")

                }
            }
            .preferredColorScheme(.dark)
            .navigationTitle("Impostazioni")
            
        }
    }
}

struct SettingView_Previews: PreviewProvider {
    static var previews: some View {
        SettingView()
    }
}
