//
//  TabView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 29/10/2020.
//

import SwiftUI

struct MainView: View {
    @State private var selection: Int = 0

    var body: some View {
        
        TabView(selection: $selection){
            
            
            HomeView()
            
            .tabItem {
                Text("Home")
                Image(systemName: "house.fill").font(.system(size: 16, weight: .regular))
                
            }.tag(0)

            RecentView()
                .tabItem {
                
                
                Text("Recenti")
                Image(systemName: "gobackward").font(.system(size: 16, weight: .regular))
            }.tag(1)
            
            ProfileView()
                .tabItem {
                Text("Profilo")
                    Image(systemName: "person.fill").font(.system(size: 16, weight: .regular))
                
            }.tag(2)
           
            
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
