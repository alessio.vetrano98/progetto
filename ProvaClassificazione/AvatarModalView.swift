//
//  AvatarModalView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 30/10/2020.
//

import SwiftUI

struct AvatarModalView: View {
    @Binding var imageAvatar: String
    
    var images = ["a1", "a2", "a3", "a4", "a5"]
    let userDefaults = UserDefaults.standard
    var dismiss: () -> ()

    var body: some View {
        
        
        Text("Select your Avatar")
            .font(.largeTitle).bold().fixedSize()

        ScrollView(.horizontal) {
            
            VStack {
            
            
            HStack {
                ForEach(0..<images.count){ index in
                    Button(action: {
                        
                        imageAvatar = images[index]
                        userDefaults.set(imageAvatar, forKey: "image")                        
                        dismiss()
                    }, label: {
                        Image("\(images[index])")
                            .resizable()
                            .frame(width: 250, height: 300, alignment: .center)
                            .padding()
                    })
                }

            }
            }
        }.padding()
        .preferredColorScheme(.dark)
    }
}

struct AvatarModalView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()
    }
}
