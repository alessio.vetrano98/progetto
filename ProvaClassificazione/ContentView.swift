//
//  ContentView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 22/10/2020.
//

import SwiftUI
import Combine
import UIKit
import CoreML


struct ContentView: View {
 
    @State private var selection: Int = 0
    
    var body: some View {
        
        TabView(selection: $selection){
            
            
            HomeView()
            
            .tabItem {
                Text("Home")
                Image(systemName: "house.fill").font(.system(size: 16, weight: .regular))
                
            }.tag(0)

            RecentView()
                .tabItem {
                
                
                Text("Recent")
                Image(systemName: "gobackward").font(.system(size: 16, weight: .regular))
            }.tag(1)
            
            ProfileView()
                
                .tabItem {
                Text("Profile")
                    Image(systemName: "person.fill").font(.system(size: 16, weight: .regular))
                
            }.tag(2)

        }
        .accentColor(Color("ColorAppGiallo"))
        
    }
    
        
    }



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
    }
}
