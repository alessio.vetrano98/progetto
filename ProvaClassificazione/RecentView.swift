//
//  RecentView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 27/10/2020.
//

import SwiftUI
import Foundation

struct RecentView: View {
    @State private var showModal = false
    @ObservedObject var getFilm = GetFilms.shared
    
    
    var body: some View {
        NavigationView {
            
            VStack{
                Divider()
                ScrollView(.horizontal){
                    
                    
                    HStack{
                        ForEach(getFilm.preferiti, id:\.name) { item in
                            
                            
                            VStack{
                                Button(action: {
                                    self.showModal.toggle()
                                    print("Numero righe: \(getFilm.preferiti.count)")
                                    
                                }){
                                    //                                    VStack {
                                    
                                    
                                    Image("\(item.name)")
                                        .resizable()
                                        .frame(width: 280 , height: 380)
                                        .cornerRadius(10)
                                        .padding()
                                    
                                }
                                Text("\(item.name)")
                                    .font(Font.body.bold() )
                                    .foregroundColor(Color.white)
                                
                                //                                Text("\(item.descriptionFilm)")
                                //                                    .multilineTextAlignment(.center)
                                //                                    .frame(width: 300, height: 200, alignment: .center)
                                //                                    .font(.system(size: 12))
                                //                                    .foregroundColor(.white)
                                
                                
                            }
                            
                            
                            .sheet(isPresented: $showModal, content: {
                                RecentModalView(filmName: item.name, filmDescription: item.descriptionFilm, platform1: item.platform1, platform2: item.platform2, platform3: item.platform3, platform4: item.platform4)
                            })
                        }
                        //                    }
                    }
                }.navigationTitle("Recent")
                .padding()
                .preferredColorScheme(.dark)
                //            .frame(height: 500)
                
                
            }
        }
    }
}

struct RecentView_Previews: PreviewProvider {
    static var previews: some View {
        RecentView()
    }
}

//func load() -> [Film] {
//
//    let path = Bundle.main.url(forResource: "data", withExtension: "json")
//    let data = try? Data(contentsOf: path!)
//    let decoder = JSONDecoder()
//    let jsonData = try? decoder.decode([Film].self, from: data!)
//
//    return jsonData!
//
//}

