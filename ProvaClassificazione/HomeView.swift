//
//  HomeView.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 28/10/2020.
//

import SwiftUI
import Combine
import UIKit
import CoreML

struct Movie: Identifiable {
    public var id = UUID()
    public var name: String
    public var filmDescription: String
    public var score: Double
}


struct HomeView: View {
    @State private var selection: Int = 0
    @ObservedObject var topRecommendations = Recommender()
    @State private var showModal = false
    @State private var selectedMovie: Movie? = nil
    @State private var searchText : String = ""
    @ObservedObject var getFilm = GetFilms.shared
    
    var selectedFilm : String = ""
    
    
    var body: some View {
        
        NavigationView {
            
            
            VStack {
                SearchBar(text: $searchText, placeholder: "Search")

                ScrollView(.horizontal) {
                    
                    HStack {


                        ForEach(topRecommendations.movies.filter {

                            movie in

                            self.searchText.isEmpty ? true: movie.name.lowercased().contains(self.searchText.lowercased())
    
                            
                        }
                        ) { movie in
                            

                            Button(action: {
                                
                                
                                selectedMovie = movie
                                self.showModal.toggle()
                                
                                
                            }) {
                                
                                VStack {
                                    
                                    Image(movie.name)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: 265 , height: 380)
                                        .cornerRadius(10)
                                        .padding()
                                    
                                    Text(movie.name)
                                        .font(Font.body.bold())
                                        .foregroundColor(Color("ColorAppGiallo"))
                                        .multilineTextAlignment(.center)
                                    
                                }
                            }.background(Color.black)
                            .frame(alignment: .center)

                            
                            
                        }
                       
                            
                        
                    } .sheet(item: $selectedMovie, content: {
                        ModalView(filmName: $0.name, filmPic: $0.name, filmDescription: $0.name, dismiss: {
                            topRecommendations.load()
                            self.selectedMovie = nil
                            
                        })
                        })
                }.padding()
            }
            
            
            .navigationBarTitle("A selection for you!")
        }.preferredColorScheme(.dark)    }
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}




struct SearchBar: UIViewRepresentable {
    
    @Binding var text: String
    var placeholder: String
    
    class Coordinator: NSObject, UISearchBarDelegate {
        
        @Binding var text: String
        
        init(text: Binding<String>) {
            _text = text
        }
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            text = searchText
            searchBar.showsCancelButton = true
            
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            text = ""
            searchBar.showsCancelButton = false
            searchBar.endEditing(true)
        }
        
    }
    
    func makeCoordinator() -> SearchBar.Coordinator {
        return Coordinator(text: $text)
    }
    
    func makeUIView(context: UIViewRepresentableContext<SearchBar>) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.delegate = context.coordinator
        searchBar.placeholder = placeholder
        searchBar.tintColor = UIColor(named:"ColorAppArancione")
        searchBar.barTintColor = UIColor(named:"ColorAppGiallo")
        //        searchBar.searchTextField.backgroundColor = UIColor(named:"ColorAppGiallo")
        searchBar.searchTextField.textColor = UIColor(named:"ColorAppGiallo")
        searchBar.searchBarStyle = .minimal
        searchBar.autocapitalizationType = .none
        return searchBar
    }
    
    func updateUIView(_ uiView: UISearchBar, context: UIViewRepresentableContext<SearchBar>) {
        uiView.text = text
    }
}



public class Recommender: ObservableObject {
    
    @Published var movies = [Movie]()
    
    init(){
        load()
    }
    
    func load() {
        
        do{
            
            let recommender = MyRecommender2()
            
            
            //            films[item].$rating = rating
            
            let ratings : [String: Double] = GetFilms.shared.preferiti.reduce([String: Double](), {
                (dict, film) in
                var d = dict
                d[film.name] = Double(film.rating!)
                return d
            })
            //            K numero di elementi da vedere
            let input = MyRecommender2Input(items: ratings, k: 5, restrict_: [], exclude: [])
            
            let result = try recommender.prediction(input: input)
            
            
            
            var tempMovies = [Movie]()
            
            for str in result.recommendations{
                
                let score = result.scores[str] ?? 0
                
                
                tempMovies.append(Movie(name: "\(str)", filmDescription: "", score: score))
                
            }
            
            self.movies = tempMovies
            
        }
        catch(let error){
            print("error is \(error.localizedDescription)")
        }
        
    }
}
