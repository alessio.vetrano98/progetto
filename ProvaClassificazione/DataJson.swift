//
//  DataJson.swift
//  ProvaClassificazione
//
//  Created by Alessio Vetrano on 28/10/2020.
//

import Foundation

struct Film2 : Codable, Hashable {
     var name: String
     var descriptionFilm: String
     var rating: Int?
     var rating2: Int?
     var platform1: String
     var platform2: String
     var platform3: String
     var platform4: String
     //     var visto: Bool? = false
}


public class GetFilms: ObservableObject {
     
     static let shared = GetFilms()
     @Published var films: [Film2] = []
     
     {
          didSet{
               let data = try! JSONEncoder().encode(films)
               UserDefaults.standard.set(data, forKey: "dataJson")
          }
     }
     
     @Published var preferiti : [Film2] = []
     
     {
          didSet{
               let data = try! JSONEncoder().encode(preferiti)
               UserDefaults.standard.set(data.base64EncodedString(), forKey: "preferiti")
          }
     }
     
     
     private init(){
          load()
          
          let stringData = UserDefaults.standard.string(forKey: "preferiti")
          if let sd = stringData {
               let data = Data(base64Encoded: sd)
               if let d = data {
                    preferiti = try! JSONDecoder().decode([Film2].self, from: d)
               }
          }
          
   
     }
     
     func load() {
          
          if let dataJson = UserDefaults.standard.data(forKey: "datajson") {
               
               do {
                    let decoder = try JSONDecoder().decode([Film2].self, from: dataJson )
                    films = decoder

               }
               catch {
                    print("error1")
               }
               
          } else if let path = Bundle.main.url(forResource: "dataFinale", withExtension: "json"){
               
               
               let data = try? Data(contentsOf: path)
               let decoder = JSONDecoder()
               do  {
                    let jsonData = try decoder.decode([Film2].self, from: data!)
                    films = jsonData
                    
                    for i in films.indices {
                         films[i].rating = 0
                    }
               }
               catch  {
                    print("error2")
               }
          }
          
          
     }
}

